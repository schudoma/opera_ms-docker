FROM ubuntu:latest

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y git wget cpanminus build-essential python3 r-base samtools spades megahit

RUN apt-get install default-jdk -y

RUN git clone --single-branch --branch OPERA-MS-0.9.0 https://github.com/CSB5/OPERA-MS.git operams

RUN wget -O samtools-0.1.19.tar.bz2 https://downloads.sourceforge.net/project/samtools/samtools/0.1.19/samtools-0.1.19.tar.bz2 && \
tar -xvf samtools-0.1.19.tar.bz2 && \
cd samtools-0.1.19 && make

#RUN wget https://github.com/voutcn/megahit/releases/download/v1.0.4-beta/megahit_v1.0.4-beta_LINUX_CPUONLY_x86_64-bin.tar.gz && \
#tar -xvzf megahit_v1.0.4-beta_LINUX_CPUONLY_x86_64-bin.tar.gz -C /usr/local/bin

#RUN wget https://github.com/voutcn/megahit/releases/download/v1.2.9/MEGAHIT-1.2.9-Linux-x86_64-static.tar.gz && \
#tar -xvzf MEGAHIT-1.2.9-Linux-x86_64-static.tar.gz && mv -v MEGAHIT-1.2.9-Linux-x86_64-static/bin/* /usr/local/bin/

# ENV PATH=megahit_v1.0.4-beta_LINUX_CPUONLY_x86_64-bin:$PATH



WORKDIR /operams

RUN sed -i \
    -e '508 s:^.\+$:run_exe("megahit " . :' \
    -e '419 s:^.\+$:my \$opera_ms_dir = \$opera_ms_option->\{\"OPERA_MS_DB_DIR\"\} . \"/tmp\"\;:' \
    -e '367 s/^.\+$/my \$opera_ms_db_dir = \$opera_ms_option->{\"OPERA_MS_DB_DIR\"};/' \
    -e "368d;" \
    -e '286 s:^.\+$:\$opera_ms_dependency->\{"perl"\} = ""\;:' \
    -e "38i \$opera_ms_option{\"OPERA_MS_DB_DIR\"} = \$ARGV[1] if (\$ARGV[0] eq \"install-db\");" \
    -e "1i \#\!\/usr\/bin\/env perl" OPERA-MS.pl && \
chmod 755 OPERA-MS.pl && \
cp -v OPERA-MS.pl /usr/local/bin/ && \
cp -rv bin /usr/local/bin/




#367 my $opera_ms_db_dir =  $opera_ms_option->{"OPERA_MS_DIR"}."/OPERA-MS-DB";
#368    $opera_ms_option->{"OPERA_MS_DB_DIR"} = $opera_ms_db_dir;


RUN make
RUN perl OPERA-MS.pl check-dependency
#RUN perl OPERA-MS.pl\
#    --contig-file test_files/contigs.fasta\
#    --short-read1 test_files/R1.fastq.gz\
#    --short-read2 test_files/R2.fastq.gz\
#    --long-read test_files/long_read.fastq\
#    --no-ref-clustering \
#    --out-dir test_files/RESULTS 2> test_files/log.err
#ENTRYPOINT ["perl", "OPERA-MS.pl"]